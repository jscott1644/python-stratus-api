import unittest

from stratus_api import ApiSession
from pprint import pprint


host = 'demo-4.videonext.com'
username = 'admin'
password = 'topse'

class TestApi(unittest.TestCase):

    def test_getObjectList_call(self):
        with ApiSession(host, username, password) as a:
            r = self.a.apiFunction('getObjectList', type='avatar', withAttributes='true')
            for avatar in r.json()['list']:
                obj = avatar['obj']
                name = avatar['attributes']['HOSTNAME']
            r = self.a.apiFunction('getObjectList', type='camera', withAttributes='true')
            for camera in r.json()['list']:
                obj = camera['obj']
                if 'AVATARID' in camera['attributes']:
                    print(obj + " belongs to " + camera['attributes']['AVATARID'])
            self.assertTrue(True)


if __name__ == '__main__':
    unittest.main()
