import os
from setuptools import setup


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = 'stratus_api',
    version = '1.0.0',
    author = 'Jeff Scott',
    author_email = 'jscott@videonext.com',
    description = ("Python bindings for Stratus API"),
    packages = ['stratus_api', 'tests'],
    test_suite = 'tests',
    install_requires=[
        'requests',
    ],
    entry_points = {
        'console_scripts': [
            'av_bootstrap=stratus_api.bootstrap:main',
            'av_replicate=stratus_api.utils:main'
        ]
    },
)
