import requests
from hashlib import sha512
from pprint import pprint

username = 'developer'
password = 'Developer@Pcs*$321'

# Create a session, for holding cookies and headers
s = requests.Session()

# get the login info
resp = s.get('http://vsaas.pcsgalaxy.net/api/call/getLoginInfo')

# set the token header, from the response (for CSRF protection)
token = resp.cookies['token']
s.headers.update({'X-token': token})

# get the encryptionKey
json = resp.json()
encryptionKey = json['loginInfo']['encryptionKey']

# generate the password digest
digestPassword = sha512(password).hexdigest()
digest = sha512(encryptionKey + digestPassword + encryptionKey).hexdigest()

# payload for login POST
data = {
    'credentials': digest,
    'name': username
}

# login POST
loginResponse = s.post('http://vsaas.pcsgalaxy.net/api/call/login', data=data)

# making an API call (pprint is a 'pretty print')
pprint(s.get('http://vsaas.pcsgalaxy.net/api/call/getObjectList').json())

# logout
s.get(apicall, params={'function': 'logout'})
