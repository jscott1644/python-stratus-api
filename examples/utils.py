import os
from ApiSession import ApiSession
import replicate


def findAvatar():
    apl_var = os.getenv('APL_VAR', '/var/avatar')
    conf_path = apl_var + "/conf/av/conf"

    with open(conf_path) as f:
        lines = f.readlines()

    ls = map(str.strip, lines)
    conf = dict(line.split("=") for line in ls)

    avatarid = conf['OBJID']
    return avatarid


def run(host, user, passwd, source, repeate=1):
    avatar = findAvatar()
    replicate.replicate(host, user, passwd, source, repeate=repeate,
        overrides={'AVATARID': avatar}
    )


# override replicate's run to inject the current avatarid
replicate.run = run

def main():
    replicate.main()


if __name__ == '__main__':
    main()
