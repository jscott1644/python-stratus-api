import requests
import hashlib

# Just too see debug stuff from the requests lib
import logging
logging.basicConfig(level=logging.DEBUG)

hostname = 'test1.vsaas.videonext.net'
username = 'avatartest'
password = '1q2w3e4r5t'

# Create a session, for holding cookies and headers
s = requests.Session()

# get the login info
resp = s.get('https://'+hostname+'/api/call/getLoginInfo')
s.headers.update({'X-token': resp.cookies['token']})

print resp.cookies

encKey = resp.json()['loginInfo']['encryptionKey'].encode()
print "EncKey: " + encKey

# generate the password digest
digestPassword = hashlib.sha512(password).hexdigest().encode()

# equivallent of sha512(encKey + sha512(pass) + encKey)
m = hashlib.sha512()
m.update(encKey)
m.update(digestPassword)
m.update(encKey)
digest = m.hexdigest()

# payload for login POST
data = {
    'credentials': digest,
    'name': username
}

# login POST
loginResponse = s.post('https://%s/api/call/login' % hostname, data=data)

print loginResponse.text
