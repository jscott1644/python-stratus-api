import replicate

host = "test1.vsaas.videonext.net"
user = "mirracle"
passwd = "!Q2w#E4r%T"
source = "1f981c4c-4109-11e9-a89b-42010a8e0002"

low = 50
high = 51

# replicates the vStream, targetting each remote camera id in the given range

for camera in xrange(low, high):
    replicate.replicate(host, user, passwd, source,
        overrides={'NAME': camera}
    )
