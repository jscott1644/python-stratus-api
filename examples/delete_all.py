from ApiSession import ApiSession
from argparse import ArgumentParser
import json
import os

import logging
logging.basicConfig(level=logging.DEBUG)

def delete_all(host, user, passwd):
    with ApiSession(host, user, passwd) as a:
        params={'type': 'camera'}
        camList = a.apiFunction('getObjectList', params=params).json()['list']

        cams = []
        for cam in camList:
            cams.append(cam['obj'])
        
        payload = {
            'objList': json.dumps(cams),
        }

        result = a.apiPost('deleteObject', data=payload)
        logging.info(result)


def run(host, user, passwd):
    # its a little weird, but it allows monkey patching easily
    delete_all(host, user, passwd)


def main():
    parser = ArgumentParser(description="Replicates the configuration of a " +
                                        "template camera to be configured " +
                                        "against the current avatar")
    parser.add_argument("hostname", help="The Stratus system's IP or Hostname")
    parser.add_argument("username", help="the admin user on Stratus")
    parser.add_argument("password", help="the password for the user")

    args = parser.parse_args()

    run(args.hostname, args.username, args.password)


if __name__ == '__main__':
    main()
