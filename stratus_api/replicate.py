from ApiSession import ApiSession
from argparse import ArgumentParser
import json
import os

import logging
logging.basicConfig(level=logging.DEBUG)

def replicate(host, user, passwd, source, repeate=1, overrides={}):
    with ApiSession(host, user, passwd) as a:
        params={'obj':source}
        attrs = a.apiFunction('getAttributes', params=params).json()['list']

        for key, value in overrides.iteritems():
            attrs[key] = value

        payload = {
            'type': 'camera',
            'attributes': json.dumps(attrs),
            'nodeid': 'e4bdedba-34c0-4bb0-95d8-8e2a43d12253' # this is avatar's UUID
        }

        for i in range(0, int(repeate)):
            result = a.apiPost('addObject', data=payload)
            logging.debug(result)


def run(host, user, passwd, source, repeate=1):
    # its a little weird, but it allows monkey patching easily
    replicate(host, user, passwd, source, repeate=repeate)


def main():
    parser = ArgumentParser(description="Replicates the configuration of a " +
                                        "template camera to be configured " +
                                        "against the current avatar")
    parser.add_argument("hostname", help="The Stratus system's IP or Hostname")
    parser.add_argument("username", help="the admin user on Stratus")
    parser.add_argument("password", help="the password for the user")
    parser.add_argument("source", help="The objid of the template camera")
    parser.add_argument("-r", "--repeate", type=int)

    args = parser.parse_args()
    repeate = 1
    if args.repeate:
        repeate = args.repeate

    run(args.hostname, args.username, args.password, args.source, repeate=repeate)


if __name__ == '__main__':
    main()
