import replicate
import utils
import os
from ConfigParser import SafeConfigParser
from argparse import ArgumentParser
from subprocess import check_call

APL = os.getenv('APL', '/opt/avatar')
APL_VAR = os.getenv('APL_VAR', '/var/avatar')
APL_CONF = os.getenv('APL_CONF', '/var/avatar/conf')

def main():
    parser = ArgumentParser()
    parser.add_argument( "config", help="Configuration file to use")
    args = parser.parse_args()

    config = SafeConfigParser({'username': 'admin', 'password': 'topse', 'template': '101'})
    config.read(args.config)

    hostname = config.get('STRATUS', 'hostname')
    username = config.get('STRATUS', 'username')
    password = config.get('STRATUS', 'password')
    template = config.get('STRATUS', 'template')

    replicas = config.get('AVATAR', 'replicas')

    activate = APL + "/bin/activate"
    activate_call = [
        activate,
        "dst="+hostname,
        "username="+username,
        "password="+password,
        "node=1",
        "options=V"
    ]
    check_call(activate_call)
    replicate.run(hostname, username, password, template, replicas)


if __name__ == '__main__':
    main()
