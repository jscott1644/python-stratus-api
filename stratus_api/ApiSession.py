import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import logging
from requests.exceptions import HTTPError
import hashlib

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

class ApiSession(requests.Session):

    def __init__(self, hostname, username, password, prefix='https'):
        self.hostname = hostname
        self.username = username
        self.password = password.encode()
        self.apicall = "%s://%s/api/call/" % (prefix, hostname)
        super(ApiSession, self).__init__()


    def __enter__(self):
        r = self.apiFunction('getLoginInfo')
        self.headers.update({'X-token': r.cookies['token']})

        encKey = r.json()['loginInfo']['encryptionKey'].encode()
        digestPassword = hashlib.sha512(self.password).hexdigest().encode()
        m = hashlib.sha512()
        m.update(encKey)
        m.update(digestPassword)
        m.update(encKey)
        digest = m.hexdigest()

        payload = {
            'credentials': digest,
            'name': self.username,
        }

        r = self.apiPost('login', data=payload)

        self.token = r.cookies['token']
        self.headers.update({'X-token': r.cookies['token']})
        self.sid = r.cookies['PHPSESSID']

        return self


    def __exit__(self, type, value, traceback):
        return self.apiFunction('logout')


    def check(self, r):
        logging.debug(r.content)
        r.raise_for_status()
        if r.json()['code'] != 200:
            logging.warn(r.url)
            logging.warn(r.content)
            raise HTTPError
        return r


    def apiPost(self, function, **kwargs):
        call = self.apicall + function
        return self.check(self.post(call, verify=False, **kwargs))


    def apiFunction(self, function, **kwargs):
        call = self.apicall + function
        return self.check(self.get(call, verify=False, **kwargs))
