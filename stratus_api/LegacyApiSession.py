import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import logging
from requests.exceptions import HTTPError
from hashlib import sha1
from xml.dom.minidom import parseString

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


class ApiSession(requests.Session):

    def __init__(self, host, username, password):
        self.host = host
        self.username = username
        self.password = password
        self.apicall = "http://"+self.host+"/api/call.php"
        super(ApiSession, self).__init__()

    def OpenSession(self):
        params={'return': 'logininfo'}
        resp = self.get("http://"+self.host+"/usr/api/login.php", params=params)
        dom = parseString(resp.text)

        for node in dom.getElementsByTagName('PARAM'):
            if node.attributes['NAME'] and node.attributes['NAME'].value == 'ENCRYPTIONKEY':
                encKey = node.attributes['VALUE'].value

        tmp = sha1(self.password).hexdigest()
        digest = sha1(encKey + tmp + encKey).hexdigest()
        params = {
            'return': 'login',
            'username': self.username,
            'credentials': digest,
        }

        r = self.get(
            "http://{}/usr/api/login.php?return=login&username={}&credentials={}".format(
                self.host,
                self.username,
                digest
        ))

        from pprint import pprint
        pprint(r.text)


    def CloseSession(self):
        r = self.get("http://"+self.host+"/usr/logout.php", params=params)


    def apiFunction(self, function, **kwargs):
        params = {'function': function}
        for key, value in kwargs.items():
            params[key] = value
        return self.get(self.apicall, params=params)

    def _sha1(self, encKey):
        m = sha1()
        m.update(encKey)
        m.update(sha1(self.password).hexdigest())
        m.update(encKey)
        return m.hexdigest()
